/* CryptoGraphJS - simplified API for Chart.js
 * Author: Tomas Fenyk (kontakt@tomasfenyk.cz)
 */

class CryptoGraphJS {
  constructor (id, chartOptions) {
    this.mainChart,
    this.overlayCanvasElement,
    this.id = id,
    this.currencies,
    this.chartElement;

    /* Basic chart options for this class and the chart itself */
    this.basicChartOptions = {
        type: 'bar',
        data: {
          labels: [],
          datasets: [],
        },
        options: {
            responsive: true,
            hoverMode: 'index',
            stacked: false,
            title:{
                display: false,
            },
            scales: {
                yAxes: [],
            },
            tooltips: {
              mode: 'x',
              intersect: false,
            },
            onHover: function(evt) {
              	return false;
            },
        },
    }

    this.createChart(id, chartOptions); // Transfers input options to basicChartOptions and creates the chart
  }

  /*
    Basic chartOptions settings structure:

    {
      chartName: "",
      xAxisData: [],
      datasets: [
        {
          borderColor: "blue",
          backgroundColor: "blue",
          fill: false,
          label: "",
          data: [],
          yAxisID: "marketCap",
      }],
      yAxises: [
        {
          type: "",
          display: true,
          position: "",
          id: "",
          scaleLabel: {
            display: true,
            labelString: "",
        }
      }],
    }

   */
  createChart (id, chartOptions) {
    if (typeof id == 'undefined') this.printLog("Missing canvas property id parameter!");
    if (typeof chartOptions == 'undefined') this.printLog("Missing input data parameter!");
    if (typeof chartOptions.datasets == 'undefined' || typeof chartOptions.chartName == 'undefined' || typeof chartOptions.xAxisData == 'undefined' ||  typeof chartOptions.currencies == 'undefined' ) this.printLog("Missing one of the chart options parameter.");

    var chartElement = document.querySelector('div#' + id),
        chartTitleElement = chartElement.querySelector('h2.title');

    this.chartElement = chartElement;

    /* Chart title */
    chartTitleElement.textContent = chartOptions.chartName;

    /* Create X axis */
    this.basicChartOptions.data.labels = chartOptions.xAxisData;

    /* Create Y axises (scales) */
    this.basicChartOptions.options.scales.yAxes = chartOptions.yAxises;

    /* Get basic info about currencies that are showed in the chart */
    this.currencies = chartOptions.currencies;

    /* Transfer events */
    this.basicChartOptions.data.events = chartOptions.events;


    /* Add events points to the chart (as bubble chart) */
    if (this.basicChartOptions.data.events.length > 0) {
      let eventTitles = [];

      /* Create event point as an object with 0 Y value */
      for (let event of this.basicChartOptions.data.events) {
        /* Find the right X coordinate as the time can differ from the ones available */
        let getDateOnScale = (eventDate) => {
          let lastDate = null,
              indexOnScale = null,
              dateLabels = this.basicChartOptions.data.labels;

          eventTitles.push(event.title);

          for (let i = 0; i < dateLabels.length; i++) {
            let availableDateObject = Date.parse(dateLabels[i]),
                eventDateObject = Date.parse(event.date);

            if (eventDateObject < availableDateObject) {
              if (i == 0 || i == (dateLabels.length - 1) || lastDate == null) {
                indexOnScale = dateLabels[i];
                break;
              } else {
                indexOnScale = dateLabels[i];
              }
            } else if (eventDateObject > availableDateObject) {
              if (i == (dateLabels.length - 1)) {
                indexOnScale = dateLabels[i];
                break;
              } else {
                continue;
              }
            } else {
              indexOnScale = dateLabels[i];
              break;
            }

            lastDate = availableDate;
          }

          return indexOnScale;
        }


        let eventDataset = {
          label: event.title,
          type: 'bubble',
          use: 'event',
          fill: false,
          backgroundColor: 'green',
          borderColor: 'lightgreen',
          data: [{x: getDateOnScale(event.date), y: 0, r: event.importance * 10}],
        };

        this.basicChartOptions.data.datasets.push(eventDataset);
      }

      /* Correctify tooltips (hide events) */
      this.basicChartOptions.options.tooltips.callbacks = {
          label: function(tooltipItems, data) {
            if (typeof data.datasets[tooltipItems.datasetIndex].use == 'undefined') {
              return ' ' + data.datasets[tooltipItems.datasetIndex].label + ': ' + data.datasets[tooltipItems.datasetIndex].data[tooltipItems.index];
            } else {
              return false;
            }
          },
          xAlign: 'left',
     }


      /* Remove events dataset labels */
      this.basicChartOptions.options.legend = { labels: {
          filter: (item, chart) => {
              return !eventTitles.includes(item.text);
          }
        }
      }
    }

    /* Transfer basic datasets
     * !! Must follow after events addition for display order !!
     */
    this.basicChartOptions.data.datasets = this.basicChartOptions.data.datasets.concat(chartOptions.datasets);

    /* Finish the chart */
    this.initializeChart(chartElement);

    console.log(this.mainChart);

    /* Daterange plugin */
    this.initializeDaterangePlugin(chartElement);

    /* Menu buttons for currencies */
    this.initializeCurrencyButtons();
  }

  initializeCurrencyButtons () {
    for (let currencyInfo of this.currencies) {
      this.addCurrencyButton(currencyInfo);
    }
  }

  addCurrencyButton (currencyOptions) {
    var currencyButton = $('<button value="' + currencyOptions.currencyId + '">' + currencyOptions.name + '</button>');
    $(currencyButton).on('click', () => {
      var removed = this.removeCryptoCurrency($(currencyButton).attr('value'));
      if (removed) $(this).remove();
    });
    $('div.currencies-list').append(currencyButton);
  }

  removeCurrencyButton (currencyId) {
    $(this.chartElement).find('div.currencies-list button[value="' + currencyId + '"]').remove();
  }

  initializeDaterangePlugin (chartElement) {
    $(chartElement).find('input[name="cryptograph-daterange"]').daterangepicker({
        ranges: {
            'Dnes': [moment(), moment()],
            'Včera': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Posledních 7 dní': [moment().subtract(6, 'days'), moment()],
            'Posledních 30 dní': [moment().subtract(29, 'days'), moment()],
            'Tento měsíc': [moment().startOf('month'), moment().endOf('month')],
            'Minulý měsíc': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        "locale": {
            "format": "DD.MM.YYYY",
            "separator": " - ",
            "applyLabel": "Potvrdit",
            "cancelLabel": "Zrušit",
            "fromLabel": "Od",
            "toLabel": "Do",
            "customRangeLabel": "Vlastní",
            "weekLabel": "T",
            "daysOfWeek": [
                "Ne",
                "Po",
                "Út",
                "St",
                "Čt",
                "Pá",
                "So"
            ],
            "monthNames": [
                "Leden",
                "Únor",
                "Březen",
                "Duben",
                "Květen",
                "Červen",
                "Červenec",
                "Srpen",
                "Září",
                "Říjen",
                "Listopad",
                "Prosinec"
            ],
            "firstDay": 1
        },
        "alwaysShowCalendars": true,
        "startDate": "25.06.2018",
        "endDate": "26.06.2018"
    }, function(start, end, label) {
      console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    });
  }

  initializeChart (chartElement) {

    var mainCanvasElement = document.querySelector('canvas#main'),
        overlayCanvasElement = document.querySelector('canvas#overlay'),
        options = this.basicChartOptions;

    /* Add colors and additional options to datasets for different currencies */
    this.generateAxisesStyles();

    /* Main chart */
    var mainChart = new Chart(mainCanvasElement, options),
        startIndex = 0,
        selectionContext = overlayCanvasElement.getContext('2d'),
        selectionRect = {
          w: 0,
          startX: 0,
          startY: 0
        },
        drag = false;

    overlayCanvasElement.width = mainCanvasElement.width;
    overlayCanvasElement.height = mainCanvasElement.height;
    this.mainChart = mainChart,
    this.overlayCanvasElement = overlayCanvasElement;

    var isEvtInChartArea = (evt) => (evt.clientX > mainChart.chartArea.left && evt.clientX < mainChart.chartArea.right);



    /* Add range selection function
     * Based on: https://stackoverflow.com/questions/42855738/how-do-i-selecting-a-date-range-like-onclick-but-drag-select
     */
    mainCanvasElement.addEventListener('pointerdown', (evt) => {
      if (isEvtInChartArea(evt)) {
        const points = mainChart.getElementsAtEventForMode(evt, 'index', {
          intersect: false
        });

        startIndex = points[0]._index;
        const rect = mainCanvasElement.getBoundingClientRect();
        selectionRect.startX = evt.clientX - rect.left;
        selectionRect.startY = mainChart.chartArea.top;
        drag = true;
      }
    });

    const rect = mainCanvasElement.getBoundingClientRect();
    mainCanvasElement.addEventListener('pointermove', (evt) => {
      if (isEvtInChartArea(evt)) {
        if (drag) {
          const rect = mainCanvasElement.getBoundingClientRect();
          selectionRect.w = (evt.clientX - rect.left) - selectionRect.startX;
          selectionContext.globalAlpha = 0.5;
          selectionContext.clearRect(0, 0, mainCanvasElement.width, mainCanvasElement.height);
          selectionContext.fillRect(selectionRect.startX,
            selectionRect.startY,
            selectionRect.w,
            mainChart.chartArea.bottom - mainChart.chartArea.top);
        } else {
          selectionContext.clearRect(0, 0, mainCanvasElement.width, mainCanvasElement.height);
          var x = evt.clientX - rect.left;

          if (x > mainChart.chartArea.left) {
            selectionContext.fillRect(x,
              mainChart.chartArea.top,
              1,
              mainChart.chartArea.bottom - mainChart.chartArea.top);
          }
        }
      }
    });

    mainCanvasElement.addEventListener('pointerup', (evt) => {
      if (isEvtInChartArea(evt)) {
        const points = mainChart.getElementsAtEventForMode(evt, 'index', {
            intersect: false
        });

        drag = false;
        console.log('implement filter between ' + options.data.labels[startIndex] + ' and ' + options.data.labels[points[0]._index]);
        }
    });

    this.chartElement.querySelector('button#change-scale-type').addEventListener('click', (evt) => {
      this.switchAxisesType();
    });

  }

  /* Removes current instance and creates a new one  */
  restartChart () {
    this.destroyChart();
    this.initializeChart(this.basicChartOptions);
  }

  /* Only refreshes the chart */
  updateChart () {
    this.mainChart.update();
  }

  destroyChart () {
    this.mainChart.destroy();
  }

  addYAxis (axisOptions) {
    /* Check for duplicities */
    if (typeof this.basicChartOptions.options.scales.yAxes.find(yAxis => yAxis.id === axisOptions.yAxisId) == 'undefined'){
      this.basicChartOptions.options.scales.yAxes.push(axisOptions.yAxisOptions);

      /* If there are datasets for the axis, add them too */
      for (let dataset of axisOptions.datasets) {
        this.basicChartOptions.data.datasets.push(dataset);
      }

      this.restartChart();
    } else {
      return false;
    }
  }

  removeYAxis (axisId) {
    /* Check for Y axis existence */
    if (typeof this.basicChartOptions.options.scales.yAxes.find(yAxis => yAxis.id === axisId) != 'undefined') {
      /* Remove the scale */
      this.basicChartOptions.options.scales.yAxes = this.basicChartOptions.options.scales.yAxes.filter((yAxis) => yAxis.id != axisId);

      /* Remove datasets for the scale */
      this.basicChartOptions.data.datasets = this.basicChartOptions.data.datasets.filter((yDataset) => yDataset.yAxisID != axisId);
      this.restartChart();
    }
  }

  /* Adds all three lines for currency */
  addCryptoCurrency (currencyOptions) {
    /* Check for currency duplication */
    if (typeof this.currencies.find(currency => currency.currencyId === currencyOptions.currency.currencyId) == 'undefined') {
      this.currencies.push(currencyOptions.currency); // Needs to preced generateAxisesStyles();

      /* Add the chart button */
      this.addCurrencyButton(currencyOptions.currency);

      /* Add values to chart and update chart */
      for (let currencyDataset of currencyOptions.datasets) {
        /* Check if all required yAxes are in the chart */
        if (typeof this.basicChartOptions.options.scales.yAxes.find(yAxis => yAxis.id === currencyDataset.yAxisID) == 'undefined') continue;

        if (typeof currencyDataset.currencyId == 'undefined') {
          currencyDataset.currencyId = currencyOptions.currency.currencyId;
        }

        this.basicChartOptions.data.datasets.push(currencyDataset);
      }

      this.generateAxisesStyles();
      this.mainChart.config = this.basicChartOptions; // Transfer color changes
      this.updateChart();

    } else {
      return false;
    }
  }

  removeCryptoCurrency (currencyId) {
    /* Check if there's more than 2 currencies, else do nothing */
    if (this.currencies.length > 1) {
      this.basicChartOptions.data.datasets = this.basicChartOptions.data.datasets.filter((yDataset) => yDataset.currencyId != currencyId);
      this.currencies = this.currencies.filter((currency) => currency.currencyId != currencyId);
      this.removeCurrencyButton(currencyId);
      this.generateAxisesStyles();
      this.updateChart();
      return true;
    } else {
      return false;
    }
  }

  /* Updates values of dataset - needs full set of values! */
  refreshDataset (currencyId, yAxisID, values) {
    this.getDatasetByCurrency(yAxisID, currencyId).data = values;
    this.updateChart();
  }

  /* Adds value to the last index of defined dataset*/
  addValueToDataSet(currencyId, yAxisID, value) {
    this.getDatasetByCurrency(yAxisID, currencyId).data.push(value);
    this.updateChart();
  }

  /* Returns dataset by yAxisID and currency name/id */
  getDatasetByCurrency (yAxisID, currencyId) {
    var foundDataset = null;

    for (let dataset of this.basicChartOptions.data.datasets) {
      if (dataset.yAxisID == yAxisID && dataset.currencyId == currencyId) {
        foundDataset = dataset;
      }
    }

    return foundDataset;
  }

  /* Switch logarithmic and linear type of axis */
  switchAxisesType () {
    for (let yAxis of this.mainChart.options.scales.yAxes) {
      if (yAxis.type == 'linear') {
        yAxis.type = 'logarithmic';
      } else {
        yAxis.type = 'linear';
      }
    }

    this.updateChart();
  }

  /* Retrieves data from backend */
  retrieveBackendData () {
      // TODO
  }

  /* Handle time range change */
  changeTimeRange (timeStart, timeEnd) {

  }

  generateAxisesStyles () {
    if (this.currencies.length == 0) {
      printLog("Cannot generate axises colors - currencies object missing!");
      return false;
    }

    var currenciesCount = 1;

    for (let currency of this.currencies) {

      /* Basic settings for axises colors */
      let basicColor =  { h: 0,
                          s: 100,
                          l: 50 };

      for (let dataset of this.basicChartOptions.data.datasets ) {
        if (dataset.currencyId == currency.currencyId) {
          if (typeof currency.currentColors == 'undefined' || typeof dataset.fullLabel == 'undefined') {
            if (typeof currency.currentColors == 'undefined') {
              currency.currentColors = {};
            }

            /* Add currency name to labels for better recognition when multiple currencies displayed */
            if (typeof dataset.fullLabel == 'undefined') {
              dataset.shortLabel = dataset.label;
              dataset.fullLabel = dataset.label + " (" + currency.name + ")";
            }
          }

          if (this.currencies.length > 1) {
            dataset.label = dataset.fullLabel;
          } else {
            dataset.label = dataset.shortLabel;
          }

          if (currenciesCount == 1) { // Preset basic colors
            if (typeof dataset.borderColor == 'undefined' && typeof dataset.backgroundColor == 'undefined') {
              if (dataset.yAxisID != 'volume') {
                dataset.borderColor = 'hsl(' + basicColor.h + ',' + basicColor.s + '%,' + basicColor.l + '%)';
                dataset.backgroundColor = 'hsl(' + basicColor.h + ',' + basicColor.s + '%,' + basicColor.l + '%)';
                dataset.hoverBackgroundColor = 'hsl(' + basicColor.h + ', 100%,' + basicColor.l + '%)';

                currency.currentColors[dataset.yAxisID] = {
                  borderColor: { h: basicColor.h, s: basicColor.s, l: basicColor.l },
                  backgroundColor: { h: basicColor.h, s: basicColor.s, l: basicColor.l },
                  hoverBackgroundColor: { h: basicColor.h, s: 100, l: basicColor.l },
                };

                if (basicColor.h >= 360) {
                 basicColor.h = basicColor.h * Math.random();
                } else {
                 basicColor.h += 205;
                }

              } else {
                dataset.borderColor = 'hsl(0, 0%, ' + (80 - this.currencies.length*10) + '%)';
                dataset.backgroundColor = 'hsl(0, 0%, ' + (80 - this.currencies.length*10) + '%)';
                dataset.hoverBackgroundColor = 'hsl(0, 0%, ' + (80 - this.currencies.length*30) + '%)';

                currency.currentColors[dataset.yAxisID] = {
                  borderColor: { h: 0, s: 0, l: (80 - this.currencies.length*10) },
                  backgroundColor: { h: 0, s: 0, l: (80 - this.currencies.length*10) },
                  hoverBackgroundColor: { h: 0, s: 0, l: (80 - this.currencies.length*30) },
                };
              }

            }
          } else if (currenciesCount > 1) { // Change only color alpha for another currency
            var alphaShift = currenciesCount * 15;

            if (dataset.yAxisID == 'volume') alphaShift = 10;

            dataset.borderColor = 'hsl(' + this.currencies[0].currentColors[dataset.yAxisID].borderColor.h + ',' + this.currencies[0].currentColors[dataset.yAxisID].borderColor.s + '%,' + (this.currencies[0].currentColors[dataset.yAxisID].borderColor.l + alphaShift) + '%)';
            dataset.backgroundColor = 'hsl(' + this.currencies[0].currentColors[dataset.yAxisID].backgroundColor.h + ',' + this.currencies[0].currentColors[dataset.yAxisID].backgroundColor.s + '%,' + (this.currencies[0].currentColors[dataset.yAxisID].backgroundColor.l + alphaShift) + '%)';
            dataset.hoverBackgroundColor = 'hsl(' + this.currencies[0].currentColors[dataset.yAxisID].hoverBackgroundColor.h + ',' + this.currencies[0].currentColors[dataset.yAxisID].hoverBackgroundColor.s + '%,' + (this.currencies[0].currentColors[dataset.yAxisID].hoverBackgroundColor.l + alphaShift) + '%)';

            currency.currentColors[dataset.yAxisID] = {
              borderColor: { h: this.currencies[0].currentColors[dataset.yAxisID].borderColor.h, s: this.currencies[0].currentColors[dataset.yAxisID].borderColor.s, l: this.currencies[0].currentColors[dataset.yAxisID].borderColor.l },
              backgroundColor: { h: this.currencies[0].currentColors[dataset.yAxisID].backgroundColor.h, s: this.currencies[0].currentColors[dataset.yAxisID].backgroundColor.s, l: this.currencies[0].currentColors[dataset.yAxisID].backgroundColor.l },
              hoverBackgroundColor: { h: this.currencies[0].currentColors[dataset.yAxisID].hoverBackgroundColor.h, s: this.currencies[0].currentColors[dataset.yAxisID].hoverBackgroundColor.s, l: this.currencies[0].currentColors[dataset.yAxisID].hoverBackgroundColor.l },
            };
          }

        }
      }

      currenciesCount++;
    }

    /* Set padding and custom styles to scales before chart initialization */
    var maxYAxisLabels = 10;

    /* Function based on: https://github.com/chartjs/Chart.js/issues/3484 */
    var calculateMax = (max) => {
      if (max % maxYAxisLabels === 0) {
        return max;
      }

      const diffDivisibleByAmountOfLabels = maxYAxisLabels - (max % maxYAxisLabels);

      return max + diffDivisibleByAmountOfLabels;
    }


    for (let yAxis of this.basicChartOptions.options.scales.yAxes) {
      for (let dataset of this.basicChartOptions.data.datasets ) {
        /* Display at least one axis with grid */
        var numberOfDatasetsWithGrid = this.basicChartOptions.options.scales.yAxes.filter((yAxis) => {
          if (typeof yAxis.gridLines == 'undefined') {
            return false;
          } else {
            if (typeof yAxis.gridLines.display == 'undefined') {
              return false;
            } else if (yAxis.gridLines.display == false) {
              return false;
            } else if (yAxis.gridLines.display != 'undefined' && yAxis.gridLines.display === true) {
              return yAxis;
            }
          }
        });

        if (dataset.yAxisID == yAxis.id) {
          /* Set margin */
          var highestValue = Math.max.apply(Math, dataset.data),
              multiplier = 1.10;

          /* Hardcoded style for volume axis */
          if (yAxis.id == 'volume') {
            multiplier = 4;
          }

          var max = Math.round(highestValue * multiplier),
              displayGridLines = false;

          if (numberOfDatasetsWithGrid.length < 1) {
            displayGridLines = true;
          }

          yAxis.gridLines = {
            display: displayGridLines,
          }

          yAxis.ticks = {
            max: calculateMax(max),
            stepSize: calculateMax(max)/maxYAxisLabels,
            callback: (value, index, values) => { // Remove the last label
              if (index == 0) {
                return null;
              } else {
                return value;
              }
            }
          };
        }

        continue;
      }
    }
  }

  printLog (text) {
    console.log("cryptoGraphJS: " + text);
  }
}
